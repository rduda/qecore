Automation of Desktop Application
=================================

Welcome to the documentation!
***************************************************


Contents:

.. toctree::
   :maxdepth: 3
   
   doc_basic_automation
   doc_qecore
 
.. only:: html 

   ******************
   Indices and Tables
   ******************

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
